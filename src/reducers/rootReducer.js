import { 
  SET_MESSAGES,
  SEND_MESSAGE, 
  EDIT_MESSAGE, 
  DELETE_MESSAGE, 
  LIKE_MESSAGE, 
  DISLIKE_MESSAGE, 
  SET_EDITING_MESSAGE_ID, 
  DROP_EDITING_MESSAGE_ID, 
  SET_ACTIVE_MESSAGE_ID,
  DROP_ACTIVE_MESSAGE_ID,
  SHOW_MODAL, 
  HIDE_MODAL, 
  HIDE_PRELOADER 
} from "../actions/actionTypes.js";
import { 
  afterLoadMapper,
  editMapper, 
  likeMapper, 
  dislikeMapper
} from "./helper.js";
import { v4 as uuidv4 } from 'uuid';

const initialState = {
  chat: {
    messages: [],
    editModal: false,
    editingMessage: '',
    activeMessage: '',
    preloader: true,
    isNeedToScroll: true,
    currentUser: {
      userId: uuidv4(),
      avatar: 'https://okeygeek.ru/wp-content/uploads/2020/03/no_avatar.png',
      user: 'Alin'
    }
  }
}

export default function rootReducer(state = initialState, action) {
    switch (action.type) {
      case SET_MESSAGES: {
        const updatedMessages = afterLoadMapper(action.payload.messages);
        return {chat: {...state.chat, messages: [...state.chat.messages, ...updatedMessages]}};
      }
      case SEND_MESSAGE: {
        const newMessage = {
          id: uuidv4(),
          userId: state.chat.currentUser.userId,
          avatar: state.chat.currentUser.avatar,
          user: state.chat.currentUser.user,
          text: action.payload.text,
          createdAt: (new Date()).toISOString(),
          editedAt: ''
        };
        return {chat: {...state.chat, messages: [...state.chat.messages, newMessage]}};
      }
      case EDIT_MESSAGE: {
        const updatedMessages = editMapper(state.chat.messages, action.payload.id, action.payload.text);
        return {chat: {...state.chat, messages: updatedMessages}};
      }
      case DELETE_MESSAGE: {
        const filteredMessages = state.chat.messages.filter(message => message.id !== action.payload.id);
        return {chat: {...state.chat, messages: filteredMessages}};
      }
      case LIKE_MESSAGE: {
        const updatedMessages = likeMapper(state.chat.messages, action.payload.id);
        return {chat: {...state.chat, messages: updatedMessages}};
      }
      case DISLIKE_MESSAGE: {
        const updatedMessages = dislikeMapper(state.chat.messages, action.payload.id);
        return {chat: {...state.chat, messages: updatedMessages}};
      }

      case SET_EDITING_MESSAGE_ID: {
        return {chat: {...state.chat, editingMessage: action.payload.id}};
      }
      case DROP_EDITING_MESSAGE_ID: {
        return {chat: {...state.chat, editingMessage: ''}};
      }
      case SET_ACTIVE_MESSAGE_ID: {
        return {chat: {...state.chat, activeMessage: action.payload.id}};
      }
      case DROP_ACTIVE_MESSAGE_ID: {
        return {chat: {...state.chat, activeMessage: ''}};
      }

      case SHOW_MODAL: {
        return {chat: {...state.chat, editModal: true}};
      }
      case HIDE_MODAL: {
        return {chat: {...state.chat, editModal: false}};
      }
      case HIDE_PRELOADER: {
        return {chat: {...state.chat, preloader: false}};
      }

      default:
          return state;
    }
}
export const afterLoadMapper = messages => {
  const updatedMessages = messages.map(message => ({
    ...message,
    type: 'other',
    isLiked: false,
    isDisliked: false
  }));
  updatedMessages.sort((a,b) => new Date(a.createdAt) - new Date(b.createdAt));
  return updatedMessages;
}

export const editMapper = (messages, id, text) => {
  const updatedMessages = messages.map(message => {
    if (message.id === id) {
        return {
            ...message,
            text
        };
    } else {
        return message;
    }
  });
  return updatedMessages;
}

export const likeMapper = (messages, id) => {
  const updatedMessages = messages.map(message => {
    if (message.id === id) {
        return {
            ...message,
            isLiked: !message.isLiked,
            isDisliked: false
        };
    } else {
        return message;
    }
});
  return updatedMessages;
}

export const dislikeMapper = (messages, id) => {
  const updatedMessages = messages.map(message => {
    if (message.id === id) {
        return {
            ...message,
            isLiked: false,
            isDisliked: !message.isDisliked
        };
    } else {
        return message;
    }
});
  return updatedMessages;
}
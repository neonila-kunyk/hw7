import { 
  SET_MESSAGES,
  SEND_MESSAGE, 
  EDIT_MESSAGE, 
  DELETE_MESSAGE, 
  LIKE_MESSAGE, 
  DISLIKE_MESSAGE, 
  SET_EDITING_MESSAGE_ID, 
  DROP_EDITING_MESSAGE_ID, 
  SET_ACTIVE_MESSAGE_ID,
  DROP_ACTIVE_MESSAGE_ID,
  SHOW_MODAL, 
  HIDE_MODAL, 
  HIDE_PRELOADER 
} from "./actionTypes";

export const setMessages = messages => ({
  type: SET_MESSAGES,
  payload: { messages }
});

export const sendMessage = text => ({
    type: SEND_MESSAGE,
    payload: { text }
});

export const editMessage = (id, text) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        text
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: { id }
});

export const likeMessage = id => ({
  type: LIKE_MESSAGE,
  payload: { id }
});

export const dislikeMessage = id => ({
  type: DISLIKE_MESSAGE,
  payload: { id }
});


export const setEditingMessageId = id => ({
    type: SET_EDITING_MESSAGE_ID,
    payload: { id }
});

export const dropEditingMessageId = () => ({
    type: DROP_EDITING_MESSAGE_ID
});

export const setActiveMessageId = id => ({
  type: SET_ACTIVE_MESSAGE_ID,
  payload: { id }
});

export const dropActiveMessageId = () => ({
  type: DROP_ACTIVE_MESSAGE_ID
});


export const showModal = () => ({
    type: SHOW_MODAL
});

export const hideModal = () => ({
    type: HIDE_MODAL
});

export const hidePreloader = () => ({
  type: HIDE_PRELOADER
});
export default async function sendRequest(url) {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  };
  const response = await fetch(url, {headers});
  if(response.ok) return response.json();
  else console.error(response.statusText);
}

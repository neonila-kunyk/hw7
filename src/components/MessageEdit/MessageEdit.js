import React from 'react';
import { connect } from 'react-redux'
import './MessageEdit.css';

import PropTypes from 'prop-types';

class MessageEdit extends React.Component {
  constructor(props) {
    super(props);
    this.textarea = React.createRef();
  }

  componentDidUpdate = (prevProps) => {
    if(this.props.editModal && this.props.input !== '' && prevProps.input === '') {
      const textarea = this.textarea.current;
      const length = this.props.input.length; 
      textarea.focus();
      setTimeout(() => textarea.setSelectionRange(length, length), 0);
    }
    
    window.addEventListener('keydown', (e) => {
      if (e.code === 'Enter') {
        this.props.onSave();
      } else if (e.code === 'Escape') {
        this.props.onCancel();
      }
    })
  }


  render() {
    const modalСlassName = 'edit-message-modal' + (this.props.editModal ? ' modal-shown' : '');
    return (
      <div className={modalСlassName}>
        <div className='modal-root'>
          <div className="modal-header">
            <h5 className="modal-title">Editing message</h5>
          </div>
          <div className="modal-body">
            <textarea 
              ref={this.textarea}
              className='edit-message-input'
              onChange={this.props.onEditChange}
              value={this.props.input}
            />
          </div>
          <div className="modal-footer">
            <button className="edit-message-close" onClick={this.props.onCancel}>Cancel</button>
            <button className="edit-message-button" onClick={this.props.onSave}>Save</button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    editModal: state.chat.editModal
  }
};

export default connect(mapStateToProps)(MessageEdit);

MessageEdit.propTypes = {
  onEditChange: PropTypes.func,
  input: PropTypes.string,
  onSave: PropTypes.func,
  onCancel: PropTypes.func
};
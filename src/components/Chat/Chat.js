import React from 'react';
import { connect } from 'react-redux'
import Preloader from '../Preloader/Preloader.js'
import Header from '../Header/Header.js'
import MessageList from '../MessageList/MessageList.js'
import MessageInput from '../MessageInput/MessageInput.js'
import MessageEdit from '../MessageEdit/MessageEdit.js'
import './Chat.css';

import sendRequest from '../../helpers/sender-req.js'
import moment from 'moment';
import PropTypes from 'prop-types';

import * as actions from '../../actions/actions.js'

class Chat extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      input: '',
      edit: ''
    }
  }

  componentDidMount() {
    sendRequest(this.props.url).then( data => {
      this.props.setMessages(data);
      this.props.hidePreloader();
    });

    window.addEventListener('keydown', (e) => {
      if (e.code === 'Delete') {

        if (!this.props.activeMessage) {
          const messages = this.props.messages.filter(message => message.type !== 'other');
          if (messages.length > 0) this.onDeleteMessage(messages[messages.length - 1].id);
        } else {
          const currentMessage = this.props.messages.find(message => message.id === this.props.activeMessage);
          const messages = [...this.props.messages].reverse();
          const currentIndex = messages.indexOf(currentMessage) || 0;
          const nextMessage = messages[currentIndex+1];
          if (nextMessage) this.props.setActiveMessageId(nextMessage.id);
          this.onDeleteMessage(currentMessage.id)
        };

      } else if (e.code === 'ArrowRight' || e.code === 'ArrowLeft') {

        const messages = [...this.props.messages].reverse();
        const currentMessage = messages.find(message => message.id === this.props.activeMessage);
        const currentIndex = messages.indexOf(currentMessage) || 0;
        const nextMessage = e.code === 'ArrowRight' ? messages[currentIndex+1] : messages[currentIndex-1];
        if (nextMessage) this.props.setActiveMessageId(nextMessage.id);

      } else if (e.code === 'ArrowUp' || e.code === 'ArrowDown') {

        if (!this.props.activeMessage) {
          const messages = this.props.messages.filter(message => message.type !== 'other');
          if (messages.length > 0) this.onEditMessage(messages[messages.length - 1].id);
        } else {
          const currentMessage = this.props.messages.find(message => message.id === this.props.activeMessage);
          if (currentMessage.type === 'other') {
            const handler = e.code === 'ArrowDown' ? this.onDislikeMessage : this.onLikeMessage;
            handler(currentMessage.id);
          } else {
            if (e.code === 'ArrowUp') this.onEditMessage(currentMessage.id);
          }
        };

      } else if (e.code === 'ControlRight' || e.code === 'AltRight') {

        const handler = e.code === 'ControlRight' ? this.onDislikeMessage : this.onLikeMessage;
        if (!this.props.activeMessage) {
          const messages = this.props.messages.filter(message => message.type === 'other');
          if (messages.length > 0 ) handler(messages[messages.length - 1].id)
        } else {
          const currentMessage = this.props.messages.find(message => message.id === this.props.activeMessage);
          handler(currentMessage.id);
        }
      }
    });
  }

  onSendMessage = () => {
    if (this.state.input !== '') {
      this.props.sendMessage(this.state.input);
      this.setState({input: ''});
    }
  } 

  onEditMessage = (id) => {
    this.props.showModal();
    this.props.setEditingMessageId(id);
    this.setState({edit: this.props.messages.find(message => message.id === id).text});
  } 

  onSave = () => {
    const editingMessage = this.props.editingMessage;
    if (this.state.edit !== '' && editingMessage) {
      const length = this.props.messages.find(message => message.id === editingMessage).text.length;
      const char = this.state.edit[length] === ' ' ? '' : ' ';
      const text = this.state.edit.substring(0, length) + char + this.state.edit.substr(length);
      this.props.editMessage(editingMessage, text);
      this.props.dropEditingMessageId();
      this.props.hideModal();
      this.setState({edit: ''});
    }
  } 

  onCancel = () => {
    this.props.dropEditingMessageId();
    this.props.hideModal();
    this.setState({edit: ''});
  }

  onDeleteMessage = (id) => {
    this.props.deleteMessage(id);
    if (this.props.messageEditing === id) this.onCancel();
  }

  onLikeMessage = (id) => {
    this.props.likeMessage(id);
  }

  onDislikeMessage = (id) => {
    this.props.dislikeMessage(id);
  }

  onInputChange = (e) => {
    this.setState({ input: e.target.value });
  };

  onEditChange = (e) => {
    this.setState({ edit: e.target.value });
  };

  getusersCount = () => {
    const usersIds = this.props.messages.map(message => message.userId);
    const usersSet = new Set(usersIds)
    return usersSet.size
  }

  getlastMessageDate = () => {
    const dates = this.props.messages.map(message => message.createdAt)
    const lastDate = dates.sort((a,b) => new Date(b) - new Date(a))[0]
    return moment(lastDate).format('DD.MM.YYYY HH:mm');
  }

  render() {
    return <div className='chat'>
      {this.props.preloader ? <Preloader /> : 
      <div className='wrapper'>
        <Header 
          chatName='Texting' 
          messagesCount={this.props.messages.length}
          usersCount={this.getusersCount()}
          lastMessageDate={this.getlastMessageDate()}
        />
        <MessageList 
          onDeleteMessage={this.onDeleteMessage}
          onEditMessage={this.onEditMessage}
          onLikeMessage={this.onLikeMessage}
          onDislikeMessage={this.onDislikeMessage}
        />
        <MessageInput 
          onInputChange={this.onInputChange} 
          input={this.state.input} 
          onSend={this.onSendMessage}
        />
        <MessageEdit 
          onEditChange={this.onEditChange} 
          input={this.state.edit} 
          onSave={this.onSave}
          onCancel={this.onCancel}
        />
      </div>}
    </div>
 }
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    editingMessage: state.chat.editingMessage,
    activeMessage: state.chat.activeMessage,
    preloader: state.chat.preloader
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);

Chat.propTypes = {
  url: PropTypes.string
};
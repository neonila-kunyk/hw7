import React from 'react';
import './OwnMessage.css';

import PropTypes from 'prop-types';

class OwnMessage extends React.Component {
  render() {
    const messageClasses = 'own-message'+ (this.props.isActive ? ' active' : '')
    return (
        <div className={messageClasses}>
          <span className='message-text'>{this.props.text}</span>
          <span className='message-time'>{this.props.time}</span>
          <i className='fas fa-cog message-edit' onClick={() => this.props.onEditMessage(this.props.id)}></i>
          <i className='fas fa-trash message-delete' onClick={() => this.props.onDeleteMessage(this.props.id)}></i>
        </div>
    )
  }
}

export default OwnMessage;

OwnMessage.propTypes = {
  text: PropTypes.string,
  time: PropTypes.string,
  onEditMessage: PropTypes.func,
  onDeleteMessage: PropTypes.func,
  id: PropTypes.string
};
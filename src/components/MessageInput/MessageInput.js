import React from 'react';
import { connect } from 'react-redux'
import './MessageInput.css';

import PropTypes from 'prop-types';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
  }

  componentDidMount = () => {
    !this.props.editModal && this.input.current.focus();

    window.addEventListener('keydown', (e) => {
      if (e.code === 'Enter') {
        this.props.onSend();
      }
    })
  }

  render() {
    return (
        <div className='message-input'>
          <input
            ref={this.input}
            className='message-input-text'
            type='text'
            placeholder='Type a message here and press Enter'
            onChange={this.props.onInputChange}
            value={this.props.input}
          />
          <button className='fa fa-paper-plane message-input-button' onClick={this.props.onSend}>
          </button>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    editModal: state.chat.editModal
  }
};


export default connect(mapStateToProps)(MessageInput);

MessageInput.propTypes = {
  onInputChange: PropTypes.func,
  input: PropTypes.string,
  onSend: PropTypes.func
};
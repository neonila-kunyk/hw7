import React from 'react';
import './Message.css';

import PropTypes from 'prop-types';

class Message extends React.Component {
  render() {
    const messageClasses = 'message'+ (this.props.isActive ? ' active' : '')
    const thumbsUpClasses = 'fas fa-thumbs-up' + (this.props.isLiked ? ' message-liked' : ' message-like')
    const thumbsDownClasses = 'fas fa-thumbs-down' + (this.props.isDisliked ? ' message-disliked' : ' message-dislike')
    return (
        <div className={messageClasses}>
          <img className='message-user-avatar' src={this.props.avatar} alt='avatar'></img>
          <span className='message-user-name'>{this.props.user}</span>
          <span className='message-text'>{this.props.text}</span>
          <span className='message-time'>{this.props.time}</span>
          <i className={thumbsUpClasses} onClick={() => this.props.onLikeMessage(this.props.id)}></i>
          <i className={thumbsDownClasses} onClick={() => this.props.onDislikeMessage(this.props.id)}></i>
        </div>
    )
  }
}

export default Message;

Message.propTypes = {
  isLiked: PropTypes.bool,
  isDisliked: PropTypes.bool,
  avatar: PropTypes.string,
  chatName: PropTypes.string,
  user: PropTypes.string,
  text: PropTypes.string,
  time: PropTypes.string,
  id: PropTypes.string,
  onLikeMessage: PropTypes.func,
  onDislikeMessage: PropTypes.func,
};
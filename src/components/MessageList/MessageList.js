import React from 'react';
import { connect } from 'react-redux'
import Message from '../Message/Message.js'
import OwnMessage from '../OwnMessage/OwnMessage.js'
import './MessageList.css';

import moment from 'moment';
import PropTypes from 'prop-types';
import ScrollToBottom from 'react-scroll-to-bottom';

class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.messageList = React.createRef();
  }

  renderItem(message, likeMessage, dislikeMessage, deleteMessage, editMessage, activeMessage) {
    if (message.type === 'other') {
      return (
        <Message
          key={message.id}
          id={message.id}
          avatar={message.avatar}
          user={message.user}
          text={message.text}
          time={moment(new Date(message.createdAt)).format('HH:mm')}
          isLiked={message.isLiked}
          isDisliked={message.isDisliked}
          onLikeMessage={likeMessage}
          onDislikeMessage={dislikeMessage}
          isActive={activeMessage === message.id ? true : false}
        />
      )
    }
    else {
      return (
        <OwnMessage
          key={message.id}
          id={message.id}
          text={message.text}
          time={moment(new Date(message.createdAt)).format('HH:mm')}
          onDeleteMessage={deleteMessage}
          onEditMessage={editMessage}
          isActive={activeMessage === message.id ? true : false}
        />
      );
    }
  }

  renderWithDivider(date) {
    const today = moment();
    const yesterday = moment().subtract(1, 'day');
    return (
      <div className='messages-divider' key={date.valueOf()}>
        <div className='divider-line'></div>
        <span className='date-label'>{   
          moment(new Date(date)).isSame(today, 'day') ? 'Today' :
          moment(new Date(date)).isSame(yesterday, 'day') ? 'Yesterday' :
          moment(new Date(date)).format('dddd, DD MMMM')
        }</span>
      </div>)
  }

  render() {
    const {messages, onDeleteMessage, onEditMessage, onLikeMessage, onDislikeMessage, activeMessage} = this.props;
    if(messages.length > 0) {
      return (
          <ScrollToBottom className='message-list'>
            <div className='message-list-items' ref={this.messageList}>
              {this.renderWithDivider(new Date(messages[0].createdAt))}
              {messages.map((message, index) => {
                if (index !== 0 && !moment(new Date(messages[index-1].createdAt)).isSame(new Date(message.createdAt), 'day')) {
                  return <React.Fragment key={index}>
                            {this.renderWithDivider(message.createdAt)}
                            {this.renderItem(message, onLikeMessage, onDislikeMessage, onDeleteMessage, onEditMessage, activeMessage)}
                         </React.Fragment>;
                } else return this.renderItem(message, onLikeMessage, onDislikeMessage, onDeleteMessage, onEditMessage, activeMessage);
              })}
            </div>
          </ScrollToBottom>
      )
    } else {
      return null;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    activeMessage: state.chat.activeMessage,
  }
};

export default connect(mapStateToProps)(MessageList);

MessageList.propTypes = {
  onDeleteMessage: PropTypes.func,
  onEditMessage: PropTypes.func,
  onLikeMessage: PropTypes.func,
  onDislikeMessage: PropTypes.func
};